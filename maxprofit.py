#given array of present values and future values, and a budget, find the maximum profit that can be made by buying and selling presents.

from typing import List
class Solution:
    def maximumProfit(self, present, future, budget):

        N = len(present)
        def maxProfit(index,budget):
            if index == N:
                return 0 #error is here
            maxProfitBuyingStock = 0
            if budget >= present[index]:
                maxProfitBuyingStock = future[index] - present[index] + maxProfit(index+1, budget - present[index])
            maxProfitNotBuyingStock = maxProfit(index + 1,budget)
            return max(maxProfitBuyingStock,maxProfitNotBuyingStock)

        return maxProfit(0,budget)
    


print(Solution().maximumProfit(present=[1,2,4,8],future=[3,5,2,9], budget = 10))
